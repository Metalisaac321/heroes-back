import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { Hero } from "models/hero";

@Controller('cats')
export class CatsController {
  @Post()
  create(@Body() createCatDto) {
    return 'This action adds a new cat';
  }

  @Get()
  findAll() {
   const cats = [
        { id: 11, name: 'Gatito 1' },
        { id: 12, name: 'Gatito 2' },
        { id: 13, name: 'Gatito 3' },
        { id: 14, name: 'Gatito 4' },
        { id: 15, name: 'Gatito 5' },
        { id: 16, name: 'Gatito 6' },
        { id: 17, name: 'Gatito 7' },
        { id: 18, name: 'Gatito 8' },
        { id: 19, name: 'Gatito 9' },
        { id: 20, name: 'Gatito 10' }
      ];
    return { cats };
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return `This action returns a #${id} cat`;
  }

  @Put(':id')
  update(@Param('id') id, @Body() updateCatDto) {
    return `This action updates a #${id} cat`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return `This action removes a #${id} cat`;
  }
}


/* 
import { Controller, Get, Post, Res, HttpStatus } from '@nestjs/common';
@Controller('cats')
export class CatsController {
  @Post()
  create(@Res() res) {
    res.status(HttpStatus.CREATED).send();
  }

  @Get()
  findAll(@Res() res) {
     res.status(HttpStatus.OK).json([]);
  }
} 

*/