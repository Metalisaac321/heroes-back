import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { Hero } from "models/hero";

const heroes: Hero[] = [
    { id: 11, name: 'Mr. Nice' },
    { id: 12, name: 'Narco' },
    { id: 13, name: 'Bombasto' },
    { id: 14, name: 'Celeritas' },
    { id: 15, name: 'Magneta' },
    { id: 16, name: 'RubberMan' },
    { id: 17, name: 'Dynama' },
    { id: 18, name: 'Dr IQ' },
    { id: 19, name: 'Magma' },
    { id: 20, name: 'Tornado' }
];

@Controller('heroes')
export class HeroesController {

  @Get()
  findAll() {
    return { heroes };
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return `This action returns a #${id} Hero`;
  }

  @Put(':id')
  update(@Param('id') id, @Body() updateHeroDto) {
    return `This action updates a #${id} Hero`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return `This action removes a #${id} Hero`;
  }
}