import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './controllers/cats.controller';
import { HeroesController } from 'controllers/heroes.controller';


@Module({
  imports: [],
  controllers: [AppController, CatsController, HeroesController],
  providers: [ AppService ]
})
export class AppModule {}
